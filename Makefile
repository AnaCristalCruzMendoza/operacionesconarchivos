#Este archivo es para compilar el programa principal
prefix=/usr/local
CC = gcc #Compilador

CFLAGS = -g -Wall #PARAMETROS
SRC = Prueba.c #ArchivoFuente
OBJ = Prueba.o #ProgramaObjeto
APP = ejecutable #NombredelBinario

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean: 
	$(RM) $(OBJ) $(APP)



